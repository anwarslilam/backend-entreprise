package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.entity.*;
import com.time2it.entrepriseservice.exception.CandidatureException;
import com.time2it.entrepriseservice.exception.OfferException;
import com.time2it.entrepriseservice.repository.CandidatureRepository;
import com.time2it.entrepriseservice.repository.InterviewRepository;
import com.time2it.entrepriseservice.repository.OfferRepository;
import com.time2it.entrepriseservice.repository.WorkflowInterviewRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CandidatureService {

    private final CandidatureRepository candidatureRepository;
    private final OfferRepository offerRepository;
    private final  MailService mailService;
    private final WorkflowInterviewRepository workflowInterviewRepository;
    private final InterviewRepository interviewRepository;

    // get candidature by offer and status (candidature who will pass interview)
    public List<Candidature> getCandidatureByOfferAndStatus(Long offerId, String status){
        Offer offer = offerRepository.findById(offerId)
                .orElseThrow(() -> new OfferException(offerId.toString()));
        List<Candidature> candidatures = candidatureRepository.findByOfferAndStatusOrderById(offer, status);
        applyWorkflow(candidatures, offer);
        List<Candidature> candidaturesFinal = candidatureRepository.findByOfferAndStatusOrderById(offer, status);
        return candidaturesFinal;
    }

    private void applyWorkflow(List<Candidature> candidatures, Offer offer) {
        List<WorkflowInterview> workflowInterviewList = workflowInterviewRepository.findAllByOfferOrderById(offer);
        for (Candidature candidature: candidatures){
            List<Interview> interviewList = interviewRepository.findAllByCandidatureOrderById(candidature);
            if(interviewList.size() <= workflowInterviewList.size()){
                for(int i=0 ; i<interviewList.size() ; i++){
                    interviewList.get(i).setTitle(workflowInterviewList.get(i).getTitle());
                    interviewList.get(i).setEvaluatorId(workflowInterviewList.get(i).getEvaluatorId());
                    interviewList.get(i).setEvaluatorName(workflowInterviewList.get(i).getEvaluatorName());
                    interviewList.get(i).setEvaluatorEmail(workflowInterviewList.get(i).getEvaluatorEmail());
                    interviewRepository.save(interviewList.get(i));
                }
                for(int i=interviewList.size(); i<workflowInterviewList.size(); i++){
                    Interview interview = new Interview();
                    interview.setCandidature(candidature);
                    interview.setEvaluatorId(workflowInterviewList.get(i).getEvaluatorId());
                    interview.setEvaluatorName(workflowInterviewList.get(i).getEvaluatorName());
                    interview.setTitle(workflowInterviewList.get(i).getTitle());
                    interview.setEvaluatorEmail(workflowInterviewList.get(i).getEvaluatorEmail());
                    interviewRepository.save(interview);
                }
            }
            else {
                for(int i=0; i<workflowInterviewList.size(); i++){
                    interviewList.get(i).setTitle(workflowInterviewList.get(i).getTitle());
                    interviewList.get(i).setEvaluatorId(workflowInterviewList.get(i).getEvaluatorId());
                    interviewList.get(i).setEvaluatorName(workflowInterviewList.get(i).getEvaluatorName());
                    interviewList.get(i).setEvaluatorEmail(workflowInterviewList.get(i).getEvaluatorEmail());
                    interviewRepository.save(interviewList.get(i));
                }
                for(int i=workflowInterviewList.size(); i<interviewList.size(); i++){
                    if(interviewList.get(i).isCreatedByWorkflow()) {
                        interviewRepository.delete(interviewList.get(i));
                    }
                }
            }
        }
    }

    public void editHiring(Candidature candidature) {
        Candidature candidature1 = candidatureRepository.findById(candidature.getId())
                .orElseThrow(() -> new CandidatureException(candidature.toString()));
        candidature1.setHiring(candidature.getHiring());
        Offer offer = candidature.getOffer();
        //Entreprise entreprise = offer.getEntreprise();
        //mailService.sendMailInterview("",entreprise,offer,candidature, null, null);
        candidatureRepository.save(candidature1);
    }
}
