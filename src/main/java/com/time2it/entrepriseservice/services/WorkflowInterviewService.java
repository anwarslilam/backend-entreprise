package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.entity.Offer;
import com.time2it.entrepriseservice.entity.WorkflowInterview;
import com.time2it.entrepriseservice.exception.InterviewException;
import com.time2it.entrepriseservice.exception.OfferException;
import com.time2it.entrepriseservice.repository.OfferRepository;
import com.time2it.entrepriseservice.repository.WorkflowInterviewRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class WorkflowInterviewService {

    private final WorkflowInterviewRepository workflowInterviewRepository;
    private final OfferRepository offerRepository;


    public List<WorkflowInterview> getWorkflow(Long offer_id){
        Offer offer = offerRepository.findById(offer_id)
                .orElseThrow(() -> new OfferException(offer_id.toString()));
        return workflowInterviewRepository.findAllByOfferOrderById(offer);
    }

    @Transactional
    public List<WorkflowInterview> saveWorkflowInterview(List<WorkflowInterview> workflowInterviews, Long offer_id){
        Offer offer = offerRepository.findById(offer_id)
                .orElseThrow(() -> new OfferException(offer_id.toString()));
        for (WorkflowInterview workflow : workflowInterviews){
            workflow.setOffer(offer);
        }
        workflowInterviewRepository.deleteAllByOffer(offer);
        return workflowInterviewRepository.saveAll(workflowInterviews);
    }

}
