package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.dto.InterviewRequest;
import com.time2it.entrepriseservice.entity.Candidature;
import com.time2it.entrepriseservice.entity.Entreprise;
import com.time2it.entrepriseservice.entity.Interview;
import com.time2it.entrepriseservice.entity.Offer;
import com.time2it.entrepriseservice.exception.InterviewException;
import com.time2it.entrepriseservice.repository.CandidatureRepository;
import com.time2it.entrepriseservice.repository.EntrepriseRepository;
import com.time2it.entrepriseservice.repository.InterviewRepository;
import com.time2it.entrepriseservice.repository.OfferRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class InterviewService {

    private final InterviewRepository interviewRepository;
    private final CandidatureRepository candidatureRepository;
    private final MailService mailService;
    private final EntrepriseRepository entrepriseRepository;
    private final OfferRepository offerRepository;

    // save new interview
    public Interview saveInterview(InterviewRequest interviewRequest){
        Interview interview = new Interview();
        Candidature candidat = candidatureRepository.findById(interviewRequest.getCandidatId())
                .orElseThrow(() -> new InterviewException(interviewRequest.toString()));
        Entreprise entreprise = entrepriseRepository.findById(interviewRequest.getEntrepriseId())
                .orElseThrow(() -> new InterviewException(interviewRequest.toString()));
        Offer offer = offerRepository.findById(interviewRequest.getOfferId())
                .orElseThrow(() -> new InterviewException(interviewRequest.toString()));
        interview.setCandidature(candidat);
        interview.setTitle(interviewRequest.getTitle());
        interview.setDateInterview(interviewRequest.getDateInterview());
        interview.setEvaluatorEmail(interviewRequest.getEvaluatorEmail());
        interview.setEvaluatorName(interviewRequest.getEvaluatorName());
        interview.setEvaluatorId(interviewRequest.getEvaluatorId());
        interview.setCreatedByWorkflow(interviewRequest.isCreatedByWorkflow());

        Interview interview1 = interviewRepository.save(interview);
        //mailService.sendMailInterview("interviewNotificationCandidat",entreprise,offer,candidat,null,interview1);
        return interview1;

    }


    // edit interview
    public Interview editInterview(InterviewRequest editedInterview){
        Interview interview = interviewRepository.findById(editedInterview.getId())
                .orElseThrow(() -> new InterviewException(editedInterview.toString()));
        interview.setTitle(editedInterview.getTitle());
        interview.setDateInterview(editedInterview.getDateInterview());
        interview.setEvaluatorEmail(editedInterview.getEvaluatorEmail());
        interview.setEvaluatorName(editedInterview.getEvaluatorName());
        interview.setEvaluatorId(editedInterview.getEvaluatorId());

        return interviewRepository.save(interview);
    }

    // edit result interview
    public Interview editResultInterview(Interview interview){
        Interview interview1 = interviewRepository.findById(interview.getId())
                .orElseThrow(() -> new InterviewException(interview.toString()));
        interview1.setResult(interview.getResult());
        interview1.setResultDescription(interview.getResultDescription());
        return interviewRepository.save(interview1);
    }

    // delete interview
    public void deleteInterview(Long id) {
        Interview interview = interviewRepository.findById(id)
                .orElseThrow(() -> new InterviewException(id.toString()));
        interviewRepository.delete(interview);
    }
}
