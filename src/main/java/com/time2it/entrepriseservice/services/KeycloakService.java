/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.entity.EntrepriseUser;
import com.time2it.entrepriseservice.entity.ResponseMessage;
import com.time2it.entrepriseservice.entity.Role;
import com.time2it.entrepriseservice.entity.User;
import java.util.ArrayList;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
/**
 *
 * @author Pavilion
 */
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@Service
public class KeycloakService {
    //private final MailService mailService;

    @Value("${keycloak.auth-server-url}") String server_url ;

    @Value("${keycloak.realm}") private String realm ;

    public Object[] createUser(User user, Long id_entreprise){
        ResponseMessage message = new ResponseMessage();
        int statusId = 0;
         try {
             UsersResource usersResource = getUsersResource();
             UserRepresentation userRepresentation = new UserRepresentation();
             userRepresentation.setUsername(user.getUsername());
             userRepresentation.setEmail(user.getEmail());
             userRepresentation.setFirstName(user.getFirstName());
             userRepresentation.setLastName(user.getLastName());
             userRepresentation.setEnabled(true);
             userRepresentation.setAttributes(Collections.singletonMap("entreprise", Arrays.asList(id_entreprise.toString())));

             Response result = usersResource.create(userRepresentation);
             statusId = result.getStatus();

             if(statusId == 201){
                 String userId = CreatedResponseUtil.getCreatedId(result);
                 //String path = result.getLocation().getPath();
                 //String userId = path.substring(path.lastIndexOf("/") + 1);
                 CredentialRepresentation passwordCredential = new CredentialRepresentation();
                 passwordCredential.setTemporary(false);
                 passwordCredential.setType(CredentialRepresentation.PASSWORD);
                 passwordCredential.setValue(user.getPassword());
                 usersResource.get(userId).resetPassword(passwordCredential);

                 RealmResource realmResource = getRealmResource();
                 RoleRepresentation roleRepresentation = realmResource.roles().get("entreprise-manager").toRepresentation();
                 realmResource.users().get(userId).roles().realmLevel().add(Arrays.asList(roleRepresentation));
                 
                 message.setMessage("User created with success");
             }else if(statusId == 409){
                 message.setMessage("Username or Email is already taken");
             }else{
                 message.setMessage("Error creating user");
             }
         }catch (Exception e){
             e.printStackTrace();
         }

         return new Object[]{statusId, message};
    }
    
    public Object[] createEntrepriseUser(EntrepriseUser entrepriseUser){
        ResponseMessage message = new ResponseMessage();
        int statusId = 0;
         try {
             UsersResource usersResource = getUsersResource();
             UserRepresentation userRepresentation = new UserRepresentation();
             userRepresentation.setUsername(entrepriseUser.getUser().getUsername());
             userRepresentation.setEmail(entrepriseUser.getUser().getEmail());
             userRepresentation.setFirstName(entrepriseUser.getUser().getFirstName());
             userRepresentation.setLastName(entrepriseUser.getUser().getLastName());
             userRepresentation.setEnabled(true);
             userRepresentation.setAttributes(Collections.singletonMap("entreprise", Arrays.asList(entrepriseUser.getEntreprise().getId().toString())));

             Response result = usersResource.create(userRepresentation);
             statusId = result.getStatus();

             if(statusId == 201){
                 String userId = CreatedResponseUtil.getCreatedId(result);
                 //String path = result.getLocation().getPath();
                 //String userId = path.substring(path.lastIndexOf("/") + 1);
                 CredentialRepresentation passwordCredential = new CredentialRepresentation();
                 passwordCredential.setTemporary(true);
                 passwordCredential.setType(CredentialRepresentation.PASSWORD);
                 passwordCredential.setValue(entrepriseUser.getUser().getPassword());
                 usersResource.get(userId).resetPassword(passwordCredential);

                 RealmResource realmResource = getRealmResource();
                 for(Role r : entrepriseUser.getRoles()){
                     RoleRepresentation roleRepresentation = realmResource.roles().get(r.getName()).toRepresentation();
                     realmResource.users().get(userId).roles().realmLevel().add(Arrays.asList(roleRepresentation));
                 }
                 message.setMessage("User created with success");
             }else if(statusId == 409){
                 message.setMessage("Username or Email is already taken");
             }else{
                 message.setMessage("Error creating user");
             }
         }catch (Exception e){
             e.printStackTrace();
         }

         return new Object[]{statusId, message};
    }
    
    public List<User> getEntrepriseUsers(Long id_entreprise){
        try {
            UsersResource usersResource = getUsersResource();
            List<UserRepresentation> list = usersResource.list();
            List<User> list2 = new ArrayList<User>();
            for(UserRepresentation u : list){
                Map<String,List<String>> M = u.getAttributes();
                
                String entr = "entreprise";
                if (M != null && M.containsKey(entr)){
                    List<String> L = M.get(entr);
                    if (L.get(0).equals(id_entreprise.toString())){
                        User u2 = new User();
                        u2.setId(u.getId());
                        u2.setUsername(u.getUsername());
                        u2.setFirstName(u.getFirstName());
                        u2.setLastName(u.getLastName());
                        u2.setEmail(u.getEmail());
                        list2.add(u2);
                    }
                } 
                
            }
            return list2;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public List<User> getAllUsers(){
        try {
            UsersResource usersResource = getUsersResource();
            List<UserRepresentation> list = usersResource.list();
            List<User> list2 = new ArrayList<User>();
            for(UserRepresentation u : list){
               User u2 = new User();
               u2.setId(u.getId());
               u2.setUsername(u.getUsername());
               u2.setFirstName(u.getFirstName());
               u2.setLastName(u.getLastName());
               u2.setEmail(u.getEmail());
               list2.add(u2); 
            }
            return list2;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    
    public User getUser(String userId){
        try {
            UsersResource usersResource = getUsersResource();
            UserRepresentation userRepresentation =usersResource.get(userId).toRepresentation();
            User user = new User();
            user.setId(userRepresentation.getId());
            user.setUsername(userRepresentation.getUsername());
            user.setFirstName(userRepresentation.getFirstName());
            user.setLastName(userRepresentation.getLastName());
            user.setEmail(userRepresentation.getEmail());
            return user;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public List<User> getEntrepriseAdmins(Long id_entreprise){
        try {
            UsersResource usersResource = getUsersResource();
            List<UserRepresentation> list = usersResource.list();
            List<User> list2 = new ArrayList<User>();
            for(UserRepresentation u : list){
                Map<String,List<String>> M = u.getAttributes();
                
                String entr = "entreprise";
                if (M != null && M.containsKey(entr)){
                    List<String> L = M.get(entr);
                    if (L.get(0).equals(id_entreprise.toString())){
                        List<RoleRepresentation> list3 = usersResource.get(u.getId()).roles().realmLevel().listAll();
                        RealmResource realmResource = getRealmResource();
                        RoleRepresentation roleRepresentation = realmResource.roles().get("entreprise-manager").toRepresentation();
                        if(list3.contains(roleRepresentation)){
                            User u2 = new User();
                            u2.setId(u.getId());
                            u2.setUsername(u.getUsername());
                            u2.setFirstName(u.getFirstName());
                            u2.setLastName(u.getLastName());
                            u2.setEmail(u.getEmail());
                            list2.add(u2);
                        }
                    }
                } 
                
            }
            return list2;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public ResponseMessage deleteEntrepriseUsers(Long id_entreprise){
        ResponseMessage message = new ResponseMessage();
        try {
            UsersResource usersResource = getUsersResource();
            List<UserRepresentation> list = usersResource.list();
            for(UserRepresentation u : list){
                Map<String,List<String>> M = u.getAttributes();
                String entr = "entreprise";
                if (M != null && M.containsKey(entr)){
                    List<String> L = M.get(entr);
                    if (L.get(0).equals(id_entreprise.toString())){
                        usersResource.delete(u.getId());
                    }
                } 
                
            }
            message.setMessage("entreprise users have been successfully deleted");

        }catch (Exception e){
            e.printStackTrace();
            message.setMessage("can't delete entreprise users !!!");
        }
        return message;
    }
    
    public ResponseMessage updateEntrepriseUser(String id, EntrepriseUser entrepriseUser){
        ResponseMessage message = new ResponseMessage();
         try {
             List<User> users = this.getAllUsers();
             
             RealmResource realmResource = getRealmResource();
             UsersResource usersResource = getUsersResource();
             
             UserRepresentation userRepresentation =usersResource.get(id).toRepresentation();
             
             for(User u : users){
                 if(u.getEmail().equalsIgnoreCase(entrepriseUser.getUser().getEmail()) && !u.getEmail().equalsIgnoreCase(userRepresentation.getEmail())){
                     message.setMessage("Email already exists !!");
                     return message;
                 }
             }
             
             userRepresentation.setFirstName(entrepriseUser.getUser().getFirstName());
             userRepresentation.setLastName(entrepriseUser.getUser().getLastName());
             userRepresentation.setEmail(entrepriseUser.getUser().getEmail());
             
             usersResource.get(id).update(userRepresentation);
             
             List<RoleRepresentation> list = usersResource.get(id).roles().realmLevel().listAll();
             usersResource.get(id).roles().realmLevel().remove(list);
             
             //realmResource.users().get(id).roles().realmLevel().listAll().clear();
             for(Role r : entrepriseUser.getRoles()){
                     RoleRepresentation roleRepresentation = realmResource.roles().get(r.getName()).toRepresentation();
                     realmResource.users().get(id).roles().realmLevel().add(Arrays.asList(roleRepresentation));
                     
             }
             message.setMessage("User updated with success.");
             
         }catch (Exception e){
             e.printStackTrace();
             message.setMessage("Error updating user !!");
         }

         return message;
    }
    
    public ResponseMessage updateUserRoles(String id, List<Role> roles){
        ResponseMessage message = new ResponseMessage();
         try {
             RealmResource realmResource = getRealmResource();
             UsersResource usersResource = getUsersResource();
             
             List<RoleRepresentation> list = usersResource.get(id).roles().realmLevel().listAll();
             usersResource.get(id).roles().realmLevel().remove(list);
             
             //realmResource.users().get(id).roles().realmLevel().listAll().clear();
             for(Role r : roles){
                     RoleRepresentation roleRepresentation = realmResource.roles().get(r.getName()).toRepresentation();
                     realmResource.users().get(id).roles().realmLevel().add(Arrays.asList(roleRepresentation));
                     
             }
             message.setMessage("user roles has been successfully updated");
             
         }catch (Exception e){
             e.printStackTrace();
             message.setMessage("can't update user roles");
         }

         return message;
    }
    
    public ResponseMessage deleteUser(String userId){
        ResponseMessage message = new ResponseMessage();
        try {
            UsersResource usersResource = getUsersResource();
            usersResource.get(userId).remove();
            
            message.setMessage("user has been successfully deleted");

        }catch (Exception e){
            e.printStackTrace();
            message.setMessage("can't delete the user");
         }
        return message;
    }
    
    public List<Role> getUserRoles(String id_user){
        try {
            UsersResource usersResource = getUsersResource();
            List<RoleRepresentation> list = usersResource.get(id_user).roles().realmLevel().listAll();
            List<Role> list2 = new ArrayList<Role>();
            for(RoleRepresentation r : list){
                if(!r.getName().equalsIgnoreCase("offline_access") && !r.getName().equalsIgnoreCase("uma_authorization") && !r.getName().equalsIgnoreCase("app_manager")){
                    Role r2 = new Role();
                
                    r2.setId(r.getId());
                    r2.setName(r.getName());
                    r2.setDescription(r.getDescription());

                    list2.add(r2);
                }
                
            }
            return list2;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public ResponseMessage createRole(Role role){
        ResponseMessage message = new ResponseMessage();
         try {
             RolesResource rolesResource = getRolesResource();
             RoleRepresentation roleRepresentation = new RoleRepresentation();
             roleRepresentation.setName(role.getName());
             roleRepresentation.setDescription(role.getDescription());
             
             List<RoleRepresentation> list = rolesResource.list();
             boolean b = true;
             for(RoleRepresentation r : list){
                if (r.getName().equals(role.getName()))
                    b = false;
             }
             
             if (b == true){
                 rolesResource.create(roleRepresentation);
                 message.setMessage("role has been successfully added");
             }
             else{
                 message.setMessage("this role already exists !!!");
             }

         }catch (Exception e){
             e.printStackTrace();
         }

         return message;
    }
    
    public List<Role> getRoles(){
        try {
            RolesResource rolesResource = getRolesResource();
            List<RoleRepresentation> list = rolesResource.list();
            List<Role> list2 = new ArrayList<Role>();
            for(RoleRepresentation r : list){
                if(!r.getName().equalsIgnoreCase("offline_access") && !r.getName().equalsIgnoreCase("uma_authorization")){
                    Role r2 = new Role();
                    r2.setId(r.getId());
                    r2.setName(r.getName());
                    r2.setDescription(r.getDescription());

                    list2.add(r2);
                }
                
            }
            return list2;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public Role getRole(String id){
        try {
            RolesResource rolesResource = getRolesResource();
            RoleRepresentation role = rolesResource.get(id).toRepresentation();
            Role r = new Role();
            r.setId(role.getId());
            r.setName(role.getName());
            r.setDescription(role.getDescription());
            return r;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public ResponseMessage updateRole(String name, Role role){
        ResponseMessage message = new ResponseMessage();
        try {
            RolesResource rolesResource = getRolesResource();
            RoleRepresentation roleToUpdate = rolesResource.get(name).toRepresentation();
            roleToUpdate.setDescription(role.getDescription());
            rolesResource.get(name).update(roleToUpdate);

            message.setMessage("role has been successfully updated");
            return message;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }
    
    public ResponseMessage deleteRole(String name){
        ResponseMessage message = new ResponseMessage();
        try {
            RolesResource rolesResource = getRolesResource();
            rolesResource.get(name).remove();
            
            message.setMessage("role has been successfully deleted");
            return message;

        }catch (Exception e){
            e.printStackTrace();
            return null;
         }
    }


    @Value("${kc.admin.username}") String keycloakUsername ;
    @Value("${kc.admin.password}") String keycloakPassword ;

    private RealmResource getRealmResource(){
        Keycloak kc = KeycloakBuilder.builder().serverUrl(server_url).realm("master").username(keycloakUsername)
                .password(keycloakPassword).clientId("admin-cli").resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build();
        return kc.realm(realm);
    }

    private UsersResource getUsersResource(){
        RealmResource realmResource = getRealmResource();
        return realmResource.users();
    }
    
    private RolesResource getRolesResource(){
        RealmResource realmResource = getRealmResource();
        return realmResource.roles();
    }
}
