package com.time2it.entrepriseservice.securite;

import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@KeycloakConfiguration
@EnableWebSecurity
public class KeycloakSpringSecuriteConfig extends KeycloakWebSecurityConfigurerAdapter {

    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(keycloakAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*
        super.configure(http);
        http
                .authorizeRequests()
                .anyRequest().permitAll();
        http.csrf().disable();
        */
        super.configure(http);
        //http.csrf().disable().authorizeRequests().antMatchers("/entreprises/**").hasAuthority("entreprise-manager");  
        http.csrf().disable().authorizeRequests()
                .antMatchers("/entreprises/**").hasAnyAuthority("app-manager","entreprise-manager")
                .antMatchers("/getEntreprises").hasAnyAuthority("app-manager")
                .antMatchers("/updateEntreprise/*").hasAnyAuthority("app-manager","entreprise-manager")
                .antMatchers("/updateEntrepriseStatut/*").hasAnyAuthority("app-manager","entreprise-manager")
                .antMatchers("/deleteEntreprise/*").hasAnyAuthority("app-manager","entreprise-manager")
                
                .antMatchers("/getRoles").hasAnyAuthority("app-manager","entreprise-manager")
                .antMatchers("/getRole/*").hasAnyAuthority("app-manager")
                .antMatchers("/createRole").hasAnyAuthority("app-manager")
                .antMatchers("/updateRole/*").hasAnyAuthority("app-manager")
                .antMatchers("/deleteRole/*").hasAnyAuthority("app-manager")
                        
                .antMatchers("/createEntrepriseUser").hasAnyAuthority("entreprise-manager")
                .antMatchers("/getEntrepriseUsers/*").hasAnyAuthority("entreprise-manager")
                .antMatchers("/getUserRoles/*").hasAnyAuthority("entreprise-manager")
                .antMatchers("/getUser/*").hasAnyAuthority("entreprise-manager")
                .antMatchers("/updateUserRoles/*").hasAnyAuthority("entreprise-manager")
                .antMatchers("/updateEntrepriseUser/*").hasAnyAuthority("entreprise-manager")
                .antMatchers("/deleteUser/*").hasAnyAuthority("entreprise-manager")
                
                .antMatchers("/createOffer/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetOffers/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetOffer/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/updateOffer/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/deleteOffer/*").hasAnyAuthority("hr-assistant")
                
                .antMatchers("/createCandidatures/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/createCandidature/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetCandidatures/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetCandidature/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/updateCandidatureStatus/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/deleteCandidature/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetOfferByCandidature/*").hasAnyAuthority("hr-assistant")
                
                .antMatchers("/createCritere/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetCriteres/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/GetCritere/*").hasAnyAuthority("hr-assistant")
                .antMatchers("/deleteCritere/*").hasAnyAuthority("hr-assistant")
                ;
        
    }
}