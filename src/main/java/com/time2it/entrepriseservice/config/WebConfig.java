package com.time2it.entrepriseservice.config;

import com.time2it.entrepriseservice.entity.Entreprise;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import javax.persistence.EntityManager;

@Configuration
public class WebConfig implements RepositoryRestConfigurer {

    private final EntityManager entityManager;

    @Value("${crossOrigin}") private String crossOrigin;

    public WebConfig(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Entreprise.class);
        
        config.getCorsRegistry()
                .addMapping("/**")
                .allowedOrigins(crossOrigin)
                .allowedMethods("GET,POST,DELETE,PUT");
    }

}

/*
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:4200")
                .allowedMethods("GET,POST,DELETE,PUT");
    }
}
*/