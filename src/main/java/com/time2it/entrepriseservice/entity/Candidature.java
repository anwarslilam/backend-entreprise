/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Pavilion
 */
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Candidature implements Serializable {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private Date applyDate;
    private String country;
    private String city;
    private Integer yearsExp;
    private String status;
    private Double score;
    // hiring accept 3 values 'not yet', yes, no
    private String hiring;
    private boolean appliedWorkflow = false;

    @ManyToOne
    @JoinColumn(name = "offer_id")
    @JsonIgnore
    private Offer offer;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidature")
    List < Interview > interviews = new ArrayList < > ();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidature")
    List < Diplome > diplomes = new ArrayList < > ();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidature")
    List < Certificat > certificats = new ArrayList < > ();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidature")
    List < Skill > skills = new ArrayList < > ();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "candidature")
    List < Experience > experiences = new ArrayList < > ();
    
}
