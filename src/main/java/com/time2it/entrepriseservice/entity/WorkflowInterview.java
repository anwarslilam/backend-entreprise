package com.time2it.entrepriseservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@AllArgsConstructor @NoArgsConstructor @Data @ToString
public class WorkflowInterview {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String evaluatorId;
    private String evaluatorEmail;
    private String evaluatorName;
    @CreationTimestamp
    private Date createdDate;

    @ManyToOne @JoinColumn(name = "offer_id")
    @JsonIgnore
    private Offer offer;
}
