package com.time2it.entrepriseservice.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Entreprise implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private String country;
    private String city;
    private String address;
    private String postalCode;
    private String phone;
    private String fax;
    private String statut;
    @Column(columnDefinition="TEXT")
    private String description;
    private String website;
    @CreationTimestamp
    private Date createdDate;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entreprise")
    List < Offer > offers = new ArrayList < > ();

}
