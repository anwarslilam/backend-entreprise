/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Offer implements Serializable {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String domaine;
    private String country;
    private String city;
    private String postalCode;
    private String address;
    private Integer experience;
    private Double salaryFrom;
    private Double salaryTo;
    private String type;
    private String status;
    private Date startDate;
    private Date expiredDate;
    @Column(columnDefinition="TEXT")
    private String description;
    @CreationTimestamp
    private Date createdDate;
    
    @ManyToOne
    @JoinColumn(name = "entreprise_id")
    @JsonIgnore
    private Entreprise entreprise;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offer")
    List < Critere > criteres = new ArrayList < > ();
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offer")
    List < Candidature > candidatures = new ArrayList < > ();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offer")
    List<WorkflowInterview>  workflowInterview;
    
    
    
}
