package com.time2it.entrepriseservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailHost {
    private String host;
    private int port;
    private String username;
    private String password;
}
