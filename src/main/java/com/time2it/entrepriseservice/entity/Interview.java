package com.time2it.entrepriseservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Interview implements Serializable {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Column(columnDefinition="TEXT")
    private String description;
    private Date dateInterview;
    // result of interview 3 values: 'not yet', 'positive', negative
    private String result = "not yet";

    //@Column(columnDefinition="VARCHAR(1000)")
    @Column(columnDefinition="TEXT")
    private String resultDescription;
    private String evaluatorEmail;
    private String evaluatorName;
    private String evaluatorId;
    @CreationTimestamp
    private Date createdDate;

    private boolean createdByWorkflow = true;
    @ManyToOne @JoinColumn(name = "candidature_id")
    @JsonIgnore
    private Candidature candidature;
}
