/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.repository;

import com.time2it.entrepriseservice.entity.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Pavilion
 */
@RepositoryRestResource
public interface CertificatRepository extends JpaRepository<Certificat, Long>{
    List<Certificat> findByCandidature(Candidature candidature);
}
