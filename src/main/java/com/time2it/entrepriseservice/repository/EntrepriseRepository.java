package com.time2it.entrepriseservice.repository;


import com.time2it.entrepriseservice.entity.Entreprise;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestParam;

@RepositoryRestResource
public interface EntrepriseRepository extends JpaRepository<Entreprise, Long> {

    Optional<Entreprise> findById(Long id);
    Page<Entreprise> findByNameContainingIgnoreCase(@RequestParam("name") String name, Pageable pageable);
    Page<Entreprise> findByStatut(@RequestParam("statut") String statut, Pageable pageable);
    Page<Entreprise> findByCountryContainingIgnoreCase(@RequestParam("country") String country, Pageable pageable);

}
