package com.time2it.entrepriseservice.repository;

import com.time2it.entrepriseservice.entity.Offer;
import com.time2it.entrepriseservice.entity.WorkflowInterview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface WorkflowInterviewRepository extends JpaRepository<WorkflowInterview, Long> {

    void deleteAllByOffer(Offer offer);
    List<WorkflowInterview> findAllByOfferOrderById(Offer offer);

}
