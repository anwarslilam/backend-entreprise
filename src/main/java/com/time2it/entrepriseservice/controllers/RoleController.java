/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.ResponseMessage;
import com.time2it.entrepriseservice.entity.Role;
import com.time2it.entrepriseservice.services.KeycloakService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Pavilion
 */

@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class RoleController {
    private final KeycloakService keycloakService;
    
    @PostMapping("/createRole")
    public ResponseEntity<ResponseMessage> createRole(@RequestBody Role role) {
        ResponseMessage res = keycloakService.createRole(role);
        return new ResponseEntity(res, HttpStatus.OK);
    }
    
    @GetMapping("/getRoles")
    public ResponseEntity<List<Role>> GetRoles(){
        return new ResponseEntity(keycloakService.getRoles(), HttpStatus.OK);
    }
    
    @GetMapping("/getRole/{name}")
    public ResponseEntity<Role> GetRole(@PathVariable("name") String name){
        return new ResponseEntity(keycloakService.getRole(name), HttpStatus.OK);
    }
    
    @PutMapping("/updateRole/{name}")
    public ResponseEntity<?> updateRole(@PathVariable("name") String name, @RequestBody Role role){
        ResponseMessage res = keycloakService.updateRole(name, role);
        return new ResponseEntity(res, HttpStatus.OK);
    }
    
    @DeleteMapping("/deleteRole/{name}")
    public ResponseEntity<?> deleteRole(@PathVariable("name") String name){
        ResponseMessage res = keycloakService.deleteRole(name);
        return new ResponseEntity(res, HttpStatus.OK);
    }
    
    
    
    
    // user roles
    
    @GetMapping("/getUserRoles/{id}")
    public ResponseEntity<List<Role>> getUserRoles(@PathVariable("id") String id_user){
        return new ResponseEntity(keycloakService.getUserRoles(id_user), HttpStatus.OK);
    }
    
}
