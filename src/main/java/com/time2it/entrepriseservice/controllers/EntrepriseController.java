/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.*;
import com.time2it.entrepriseservice.repository.EntrepriseRepository;
import com.time2it.entrepriseservice.services.KeycloakService;
import com.time2it.entrepriseservice.services.MailService;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Pavilion
 */
@RestController
@AllArgsConstructor
@Slf4j
@CrossOrigin("*")
public class EntrepriseController {
    private final EntrepriseRepository entrepriseRepo;
    private final MailService mailService;
    private final KeycloakService keycloakService;
    
    
    @GetMapping("/getEntreprises")
    public ResponseEntity<List<Entreprise>> GetEntreprises(){
        return new ResponseEntity(entrepriseRepo.findAll(), HttpStatus.OK);
    }
    
    @GetMapping("/getEntreprise/{id}")
    public ResponseEntity<Entreprise> GetEntreprise(@PathVariable("id") Long id){
        Entreprise entreprise = entrepriseRepo.findById(id).orElse(null);
        return new ResponseEntity(entreprise, HttpStatus.OK);
    }
    
    @PostMapping("/createEntreprise")
    public ResponseEntity<ResponseMessage> createEntreprise(@RequestBody Dto dto) {
        Entreprise entr = entrepriseRepo.save(dto.getEntreprise());
        User usr = dto.getUser();
        Object[] obj = keycloakService.createUser(usr, entr.getId());
        int status = (int) obj[0];
        //if user created with success
        if(status == 201){
            mailService.sendRegistrationMail(usr.getEmail());
            mailService.sendAdminMail(entr);
        }
        else{
            entrepriseRepo.delete(entr);
        }
        ResponseMessage message = (ResponseMessage) obj[1];
        return ResponseEntity.status(status).body(message);
    }

    @PostMapping("/createEntrepriseByTime2it")
    public ResponseEntity<Entreprise> createEntreprise2(@RequestBody Dto dto) {
        Entreprise entr = entrepriseRepo.save(dto.getEntreprise());
        User usr = dto.getUser();
        Object[] obj = keycloakService.createUser(usr, entr.getId());
        int status = (int) obj[0];
        mailService.sendRegistrationMail(usr.getEmail());
        return ResponseEntity.status(status).body(entr);
    }
    
    @PutMapping("/updateEntreprise/{id}")
    public ResponseEntity<Entreprise> update(@PathVariable("id") Long id, @RequestBody Entreprise entr){
        Entreprise entreprise = entrepriseRepo.findById(id).get();
        entreprise.setName(entr.getName());
        entreprise.setEmail(entr.getEmail());
        entreprise.setCountry(entr.getCountry());
        entreprise.setCity(entr.getCity());
        entreprise.setAddress(entr.getAddress());
        entreprise.setPostalCode(entr.getPostalCode());
        entreprise.setPhone(entr.getPhone());
        entreprise.setFax(entr.getFax());
        entreprise.setDescription(entr.getDescription());
        entreprise.setWebsite(entr.getWebsite());
        Entreprise updatedEntreprise = entrepriseRepo.save(entreprise);
        return new ResponseEntity(updatedEntreprise, HttpStatus.OK);
    }
    
    @PutMapping("/updateEntrepriseStatut/{id}")
    public ResponseEntity<Entreprise> updateEntrepriseStatut(@PathVariable("id") Long id, @RequestBody Entreprise entr){
        Entreprise entreprise = entrepriseRepo.findById(id).get();
        entreprise.setStatut(entr.getStatut());
        Entreprise updatedEntreprise = entrepriseRepo.save(entreprise);
        if(entr.getStatut().equalsIgnoreCase("accepted")){
            mailService.sendEntrepriseStatutEditionMail("Demande acceptée", entr.getEmail(), "Votre demande de création d'entreprise a été acceptée", "accéder au plateform");
        }
        if(entr.getStatut().equalsIgnoreCase("refused")){
            mailService.sendEntrepriseStatutEditionMail("Demande refusée", entr.getEmail(), "Votre demande de création d'entreprise a été refusée", "envoyer une nouvelle demande");
        }
        return new ResponseEntity(updatedEntreprise, HttpStatus.OK);
    }
    
    @DeleteMapping("/deleteEntreprise/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        Entreprise entreprise = entrepriseRepo.findById(id).get();
        entrepriseRepo.delete(entreprise);
        keycloakService.deleteEntrepriseUsers(id);
        return new ResponseEntity(new ResponseMessage("entreprise "+entreprise.getName()+" a été supprimée"), HttpStatus.OK);
    }
    
    // Entreprise user methodes
    
    @PostMapping("/createEntrepriseUser")
    public ResponseEntity<ResponseMessage> createEntrepriseUser(@RequestBody EntrepriseUser entrepriseUser) {
        Object[] obj = keycloakService.createEntrepriseUser(entrepriseUser);
        int status = (int) obj[0];
        String entreprise = this.GetEntreprise(entrepriseUser.getEntreprise().getId()).getBody().getName();
        if(status == 201){
            mailService.sendNewEntrepriseUserMail(entrepriseUser.getUser().getEmail(), entrepriseUser.getUser().getPassword(), entreprise, entrepriseUser.getRoles());
        }
        ResponseMessage message = (ResponseMessage) obj[1];
        return ResponseEntity.status(status).body(message);
    }
    
    @GetMapping("/getEntrepriseUsers/{id}")
    public ResponseEntity<List<User>> GetEntrepriseUsers(@PathVariable("id") Long id){
        return new ResponseEntity(keycloakService.getEntrepriseUsers(id), HttpStatus.OK);
    }
    
    
    @GetMapping("/getUser/{id}")
    public ResponseEntity<User> GetUser(@PathVariable("id") String id){
        return new ResponseEntity(keycloakService.getUser(id), HttpStatus.OK);
    }
    
    @DeleteMapping("/deleteUser/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") String id){
        ResponseMessage res = keycloakService.deleteUser(id);
        return new ResponseEntity(res, HttpStatus.OK);
    }
    
    @PutMapping("/updateUserRoles/{id}")
    public ResponseEntity<ResponseMessage> updateUserRoles(@PathVariable("id") String id, @RequestBody List<Role> roles) {
        ResponseMessage res = keycloakService.updateUserRoles(id, roles);
        return new ResponseEntity(res, HttpStatus.OK);
    }
    
    @PutMapping("/updateEntrepriseUser/{id}")
    public ResponseEntity<ResponseMessage> updateEntrepriseUser(@PathVariable("id") String id, @RequestBody EntrepriseUser entrepriseUser) {
        ResponseMessage res = keycloakService.updateEntrepriseUser(id, entrepriseUser);
        return new ResponseEntity(res, HttpStatus.OK);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    @GetMapping("/getEmailAdmin")
    public ResponseEntity<String> getAdminEmail(){
        String email = mailService.getAdminEmail();
        return new ResponseEntity<>(email,HttpStatus.OK);
    }
    @PutMapping("/updateEmailAdmin/{newEmail}")
    public ResponseEntity<String> updateAdminEmail(@PathVariable String newEmail){
        mailService.updateAdminEmail(newEmail);
        return new ResponseEntity<>("email mis à jour avec succès",HttpStatus.OK);
    }
    @GetMapping("/getEmailHost")
    public ResponseEntity<EmailHost> getEmailHost(){
        EmailHost emailHost = mailService.getEmailHostProperties();
        return new ResponseEntity<>(emailHost,HttpStatus.OK);
    }
    @PutMapping("/updateEmailHost")
    public ResponseEntity<String> setEmailHost(@RequestBody EmailHost emailHost){
        mailService.updateEmailHostProperties(emailHost);
        return new ResponseEntity<>("Host email mis à jour avec succès",HttpStatus.OK);

    }
    
            
    
}
