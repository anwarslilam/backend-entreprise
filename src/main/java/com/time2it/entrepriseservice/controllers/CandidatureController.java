package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.Candidature;
import com.time2it.entrepriseservice.entity.Certificat;
import com.time2it.entrepriseservice.entity.Diplome;
import com.time2it.entrepriseservice.entity.Experience;
import com.time2it.entrepriseservice.entity.Offer;
import com.time2it.entrepriseservice.entity.ResponseMessage;
import com.time2it.entrepriseservice.entity.Skill;
import com.time2it.entrepriseservice.repository.CandidatureRepository;
import com.time2it.entrepriseservice.repository.CertificatRepository;
import com.time2it.entrepriseservice.repository.DiplomeRepository;
import com.time2it.entrepriseservice.repository.ExperienceRepository;
import com.time2it.entrepriseservice.repository.OfferRepository;
import com.time2it.entrepriseservice.repository.SkillRepository;
import java.util.List;

import com.time2it.entrepriseservice.services.CandidatureService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Pavilion
 */
@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class CandidatureController {

    private final CandidatureRepository candidatureRepo;
    private final OfferRepository offerRepo;
    private final CertificatRepository certificatRepo;
    private final DiplomeRepository diplomeRepo;
    private final SkillRepository skillRepo;
    private final ExperienceRepository experienceRepo;

    private final CandidatureService candidatureService;

    @GetMapping("/GetCandidatures/{id_offer}")
    public ResponseEntity<List<Candidature>> GetCandidatures(@PathVariable("id_offer") Long id_offer) {
        //List<Candidature> list = candidatureRepo.findByOffer(offerRepo.findById(id_offer).get());
        List<Candidature> list = offerRepo.findById(id_offer).get().getCandidatures();
        return new ResponseEntity(list, HttpStatus.OK);
    }

    @GetMapping("/GetCandidature/{id}")
    public ResponseEntity<Candidature> GetCandidature(@PathVariable("id") Long id) {
        Candidature candidature = candidatureRepo.findById(id).get();
        return new ResponseEntity(candidature, HttpStatus.OK);
    }

    @PostMapping("/createCandidature/{id_offer}")
    public ResponseEntity<Candidature> createCandidature(@PathVariable("id_offer") Long id_offer, @RequestBody Candidature candidature) {
        Offer offer = offerRepo.findById(id_offer).get();
        candidature.setOffer(offer);
        Candidature candidature2 = candidatureRepo.save(candidature);

        return new ResponseEntity(candidature2, HttpStatus.OK);
    }

    @PostMapping("/createCandidatures/{id_offer}")
    public void createCandidatures(@PathVariable("id_offer") Long id_offer, @RequestBody List<Candidature> candidatures) {
        Offer offer = offerRepo.findById(id_offer).get();
        for (Candidature c : candidatures) {
            //boolean EmptyOrExists = false;
            //List<Candidature> list = offerRepo.findById(id_offer).get().getCandidatures();

            List<Certificat> certificats = c.getCertificats();
            List<Diplome> diplomes = c.getDiplomes();
            List<Experience> experiences = c.getExperiences();
            List<Skill> skills = c.getSkills();

            //List<Critere> criteres = offerRepo.findById(id_offer).get().getCriteres();

            c.setCertificats(null);
            c.setDiplomes(null);
            c.setExperiences(null);
            c.setSkills(null);
            c.setOffer(offer);
            Candidature c2 = candidatureRepo.save(c);

            for (Certificat cer : certificats) {
                cer.setCandidature(c2);
                certificatRepo.save(cer);
            }
            for (Diplome d : diplomes) {
                d.setCandidature(c2);
                diplomeRepo.save(d);
            }
            for (Experience e : experiences) {
                e.setCandidature(c2);
                experienceRepo.save(e);
            }
            for (Skill s : skills) {
                s.setCandidature(c2);
                skillRepo.save(s);
            }
            /*
            for(Candidature ca : list){
                if(c.getEmail() == null || c.getEmail().equalsIgnoreCase(ca.getEmail())){
                    EmptyOrExists = true;
                }
            }
            if(!EmptyOrExists){

            }
            */
        }
    }

    @PutMapping("/updateCandidatureStatus/{id}")
    public ResponseEntity<?> updateCandidatureStatus(@PathVariable("id") Long id, @RequestBody Candidature c) {
        Candidature candidature = candidatureRepo.findById(id).get();
        candidature.setStatus(c.getStatus());
        candidatureRepo.save(candidature);

        return new ResponseEntity(new ResponseMessage("Candidature status has been successfully updated."), HttpStatus.OK);
    }

    @DeleteMapping("/deleteCandidature/{id}")
    public ResponseEntity<?> deleteCandidature(@PathVariable("id") Long id) {
        Candidature candidature = candidatureRepo.findById(id).get();
        candidatureRepo.delete(candidature);

        return new ResponseEntity(new ResponseMessage("Offer " + candidature.getFirstName() + " " + candidature.getLastName() + " has been successfully deleted."), HttpStatus.OK);
    }

    @GetMapping("/GetOfferByCandidature/{id_candidature}")
    public ResponseEntity<Offer> GetOfferByCandidature(@PathVariable("id_candidature") Long id_candidature) {
        Candidature candidature = candidatureRepo.findById(id_candidature).get();
        return new ResponseEntity(candidature.getOffer(), HttpStatus.OK);
    }

    // method for manage interviews
    @GetMapping("/getCandidatureByOfferAndStatus/{offer_id}/{status}")
    public ResponseEntity<List<Candidature>> getCandidatureByOfferAndStatus
    (@PathVariable Long offer_id, @PathVariable String status) {
        List<Candidature> candidatures = candidatureService.getCandidatureByOfferAndStatus(offer_id, status);
        return new ResponseEntity(candidatures, HttpStatus.OK);
    }

    // method for manage interviews
    @PutMapping("/edit-hiring-decision")
    public ResponseEntity<Void> editDecisionHiring(@RequestBody Candidature candidature) {
        candidatureService.editHiring(candidature);
        return new ResponseEntity(HttpStatus.OK);
    }
}
