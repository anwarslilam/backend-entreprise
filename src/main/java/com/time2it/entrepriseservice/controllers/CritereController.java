/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.Candidature;
import com.time2it.entrepriseservice.entity.Critere;
import com.time2it.entrepriseservice.entity.Offer;
import com.time2it.entrepriseservice.entity.ResponseMessage;
import com.time2it.entrepriseservice.entity.Skill;
import com.time2it.entrepriseservice.repository.CandidatureRepository;
import com.time2it.entrepriseservice.repository.CritereRepository;
import com.time2it.entrepriseservice.repository.OfferRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Pavilion
 */
@RestController
@AllArgsConstructor
@Slf4j
@CrossOrigin("*")
public class CritereController {
    private final CritereRepository critereRepo;
    private final OfferRepository offerRepo;
    private final CandidatureRepository candidatureRepo;
    
    @GetMapping("/GetCriteres/{id_offer}")
    public ResponseEntity<List<Critere>> GetCriteres(@PathVariable("id_offer") Long id_offer){
        List<Critere> list = offerRepo.findById(id_offer).get().getCriteres();
        return new ResponseEntity(list, HttpStatus.OK);  
    }
    
    @GetMapping("/GetCritere/{id}")
    public ResponseEntity<Critere> GetCritere(@PathVariable("id") Long id){
        Critere critere = critereRepo.findById(id).get();
        return new ResponseEntity(critere, HttpStatus.OK);
    }
    
    @PostMapping("/createCritere/{id_offer}")
    public ResponseEntity<Critere> createCritere(@PathVariable("id_offer") Long id_offer, @RequestBody Critere critere) {
        Offer offer = offerRepo.findById(id_offer).get();
        critere.setOffer(offer);
        Critere cr = critereRepo.save(critere);
        
        List<Candidature> candidatures = offerRepo.findById(id_offer).get().getCandidatures();
        for(Candidature c : candidatures){
            for(Skill s : c.getSkills()){
                if(s.getName().equalsIgnoreCase(critere.getName())){
                    c.setScore(c.getScore() + critere.getWeight());
                    break;
                }
            }
            candidatureRepo.save(c);
        }

        return new ResponseEntity(cr, HttpStatus.OK);
    }
    
    
    @DeleteMapping("/deleteCritere/{id}")
    public ResponseEntity<?> deleteCritere(@PathVariable("id") Long id){
        Critere critere = critereRepo.findById(id).get();
        Offer offer = offerRepo.findById(critere.getOffer().getId()).get();
        List<Candidature> candidatures = offerRepo.findById(offer.getId()).get().getCandidatures();
        for(Candidature c : candidatures){
            for(Skill s : c.getSkills()){
                if(s.getName().equalsIgnoreCase(critere.getName())){
                    c.setScore(c.getScore() - critere.getWeight());
                    break;
                }
            }
            candidatureRepo.save(c);
        }
        critereRepo.delete(critere);
        
        return new ResponseEntity(new ResponseMessage("Critere " +critere.getName()+"  has been successfully deleted."), HttpStatus.OK);
    }
}
