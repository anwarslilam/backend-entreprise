/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.Entreprise;
import com.time2it.entrepriseservice.entity.Offer;
import com.time2it.entrepriseservice.entity.ResponseMessage;
import com.time2it.entrepriseservice.repository.EntrepriseRepository;
import com.time2it.entrepriseservice.repository.OfferRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Pavilion
 */
@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class OfferController {
    private final OfferRepository offerRepo ;
    private final EntrepriseRepository entrepriseRepo ;
    
    @GetMapping("/GetOffers/{id_entreprise}")
    public ResponseEntity<List<Offer>> GetOffers(@PathVariable("id_entreprise") Long id_entreprise){
        List<Offer> list = offerRepo.findByEntrepriseOrderByCreatedDateDesc(entrepriseRepo.findById(id_entreprise).get());
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/GetOffer/{id}")
    public ResponseEntity<Offer> GetOffer(@PathVariable("id") Long id){
        Offer offer = offerRepo.findById(id).get();
        return new ResponseEntity(offer, HttpStatus.OK);
    }
    
    @PostMapping("/createOffer/{id_entreprise}")
    public ResponseEntity<Offer> createOffer(@PathVariable("id_entreprise") Long id_entreprise, @RequestBody Offer offer) {
        Entreprise entreprise = entrepriseRepo.findById(id_entreprise).get();
        offer.setEntreprise(entreprise);
        Offer offer2 = offerRepo.save(offer);

        return new ResponseEntity(offer2, HttpStatus.OK);
    }
    
    @PutMapping("/updateOffer/{id}")
    public ResponseEntity<?> updateOffer(@PathVariable("id") Long id, @RequestBody Offer o){
        Offer offer = offerRepo.findById(id).get();
        offer.setTitle(o.getTitle());
        offer.setDomaine(o.getDomaine());
        offer.setCountry(o.getCountry());
        offer.setCity(o.getCity());
        offer.setPostalCode(o.getPostalCode());
        offer.setAddress(o.getAddress());
        offer.setExperience(o.getExperience());
        offer.setSalaryFrom(o.getSalaryFrom());
        offer.setSalaryTo(o.getSalaryTo());
        offer.setType(o.getType());
        offer.setStatus(o.getStatus());
        offer.setStartDate(o.getStartDate());
        offer.setExpiredDate(o.getExpiredDate());
        offer.setDescription(o.getDescription());
        offerRepo.save(offer);

        return new ResponseEntity(new ResponseMessage("Offer "+offer.getTitle()+" has been successfully updated."), HttpStatus.OK);
    }
    
    @DeleteMapping("/deleteOffer/{id}")
    public ResponseEntity<?> deleteOffer(@PathVariable("id") Long id){
        Offer offer = offerRepo.findById(id).get();
        offerRepo.delete(offer);
        
        return new ResponseEntity(new ResponseMessage("Offer "+offer.getTitle()+" has been successfully deleted."), HttpStatus.OK);
    }
}
