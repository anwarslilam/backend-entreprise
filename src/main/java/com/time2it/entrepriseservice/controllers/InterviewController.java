package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.dto.InterviewRequest;
import com.time2it.entrepriseservice.entity.*;
import com.time2it.entrepriseservice.services.InterviewService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Slf4j
public class InterviewController {

    private final InterviewService interviewService;

    @PostMapping("/add-interview/{idEntreprise}")
    public ResponseEntity<Interview> addInterview(@RequestBody InterviewRequest interviewRequest) {
        Interview interview = interviewService.saveInterview(interviewRequest);
        return new ResponseEntity<>(interview, HttpStatus.CREATED);
    }

    @PutMapping("/edit-interview")
    public ResponseEntity<Interview> editInterview(@RequestBody InterviewRequest interviewRequest){
        Interview interview1 = interviewService.editInterview(interviewRequest);
        return new ResponseEntity<>(interview1, HttpStatus.OK);
    }

    @PatchMapping("/edit-result-interview")
    public ResponseEntity<Interview> editResultInterview(@RequestBody Interview interview){
        Interview interview1 = interviewService.editResultInterview(interview);
        return new ResponseEntity<>(interview1, HttpStatus.OK);
    }

    @DeleteMapping("/delete-interview/{id}")
    public ResponseEntity<Void> deleteInterview(@PathVariable Long id){
        interviewService.deleteInterview(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
