package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.WorkflowInterview;
import com.time2it.entrepriseservice.services.WorkflowInterviewService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class WorkflowInterviewController {

    private final WorkflowInterviewService workflowInterviewService;

    @GetMapping("/get-workflow-interviews/{offer_id}")
    public ResponseEntity<List<WorkflowInterview> > getWorkflow(@PathVariable Long offer_id){
        List<WorkflowInterview> list = workflowInterviewService.getWorkflow(offer_id);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/add-workflow-interview/{offer_id}")
    public ResponseEntity<List<WorkflowInterview>> addWorkflowInterview
            (@RequestBody List<WorkflowInterview> workflowInterviews, @PathVariable Long offer_id){
        List<WorkflowInterview> list = workflowInterviewService.saveWorkflowInterview(workflowInterviews, offer_id);
        return new ResponseEntity<>(list, HttpStatus.CREATED);
    }

}
