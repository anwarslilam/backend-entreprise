package com.time2it.entrepriseservice.exception;

public class InterviewException extends RuntimeException {

    public InterviewException(String message){
        super(message);
    }

}
