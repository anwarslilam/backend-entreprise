package com.time2it.entrepriseservice.exception;

public class CandidatureException extends RuntimeException {

    public CandidatureException(String message){
        super(message);
    }
}
