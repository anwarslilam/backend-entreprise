package com.time2it.entrepriseservice.exception;

public class OfferException extends RuntimeException{

    public OfferException(String message){
        super(message);
    }

}
