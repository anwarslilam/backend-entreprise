package com.time2it.entrepriseservice.exception;

public class EntrepriseException extends RuntimeException {

    public EntrepriseException(String exMessage, Exception exception) {
        super(exMessage, exception);
    }
    public EntrepriseException(String exMessage) {
        super(exMessage);
    }

}
