package com.time2it.entrepriseservice.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
@Data @AllArgsConstructor @NoArgsConstructor
@ToString
public class InterviewRequest {
    private Long id;
    private String title;
    private Date dateInterview;
    private String result;
    private String resultDescription;
    private String evaluatorEmail;
    private String evaluatorName;
    private String evaluatorId;
    private Long candidatId;
    private Long offerId;
    private Long entrepriseId;
    private boolean createdByWorkflow;

}
